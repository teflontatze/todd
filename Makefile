CFLAGS=-Wextra -Wall -Werror -pedantic -std=c11 -g
CC=cc

EXE=todd

SRCS=src/todd.c

.PHONY: all run clean install

all: $(EXE)

run: $(EXE)
	./$(EXE)

install: $(EXE)
	cp $(EXE) ~/.local/bin/

clean:
	$(RM) $(EXE)

$(EXE): $(SRCS)
	$(CC) $(CFLAGS) -o $@ $<
