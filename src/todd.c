/*** Includes ***/

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

/*** Macros ***/

#define LEN(x) ((sizeof(x)) / (sizeof(*x)))


/*** Defines ***/

#define MAX_CMD 4096
#define MAX_PATH 2048

/*** Types ***/

typedef struct todo {
	char const *task;
} Todo;

/*** Globals ***/

char data_dir[1024];
char *editor;

/*** Arguments ***/

int project = 0;
int daily = 0;

/*** Misc ***/

void usage(char const *const name) {
	printf("Usage %s:\n", name);
	printf("    -h\tPrint this message and exit signaling success\n");
	printf("    -d\tOpen daily note in $EDITOR\n");
}

/*** Environment handling ***/

char const *get_preferred_editor() {
	char *e;
	return (e = getenv("EDITOR"))? e : "vi";
}

/*** File and Directory handlig ***/

int directory_exists(char const *const path) {
	struct stat s;
	if (stat(path, &s) < 0)
		return 0;
	if (S_ISDIR(s.st_mode))
		return 1;
	return 0;
}

int directory_create(char const *const path) {
	// Check if directory exists
	if (directory_exists(path))
		return 1;
	if (mkdir(path, S_IRWXU) < 0)
		return 0;
	return 1;
}

int file_create(char const *const path) {
	int fd = 0;
	if ((fd = open(path, O_CREAT, S_IRUSR | S_IWUSR)) < 0)
		return 0;

	close(fd);
	return 1;
}

/*** Setup ***/

int write_data_dir_to_buffer(char buf[], unsigned long bufsize) {
	char *data_home = getenv("XDG_DATA_HOME");
	if (!data_home) {
		char *home = getenv("HOME");
		if (!home) {
			return 0;
		}
		if (snprintf(buf, bufsize, "%s/.local/share/todd/", home) < 0)
			return 0;
	}
	return 1;
}

int todd_init() {
	if (!write_data_dir_to_buffer(data_dir, sizeof(data_dir)))
		return 0;
	if (!directory_create(data_dir)) {
		return 0;
	}
	editor = get_preferred_editor();
	return 1;
}

/*** Daily Notes ***/

int daily_note(char const *date) {
	// Year as four digit number + '\0'
	char dir[5];
	// Month as two digit number + '-' + day as two digit number + '\0'
	char file[6];

	// FIXME: We do not account for the unlikely case we determine the name of the directory for daily notes
	// before a new year begins but the name for the file after a new year begins. Figure something out for
	// this edge-case.
	
	if (!date) {
		time_t m_time = time(NULL);
		if (m_time == (time_t)-1)
			return 0;
		
		struct tm *t = localtime(&m_time);
		if (!t)
			return 0;
		
		if (strftime(dir, LEN(dir), "%Y", t) == 0)
			return 0;
		
		// Account for the unlikely case we cross midnight
		m_time = time(NULL);
		if (m_time == (time_t)-1)
			return 0;

		t = localtime(&m_time);
		if (!t)
			return 0;

		if (strftime(file, LEN(file), "%m-%d", t) == 0)
			return 0;
	} else {
		return 0;
	}

	char cmd[MAX_CMD];

	char path[MAX_PATH];	
	snprintf(path, MAX_PATH, "%s%s/", data_dir, dir);

	if (!directory_create(path)) {
		return 0;
	}

	snprintf(cmd, LEN(cmd), "%s %s%s.md", editor, path, file);
	return system(cmd);
}

/*** Project Notes ***/

int project_note(char const *const project_name) {
	char path[MAX_PATH];	
	snprintf(path, MAX_PATH, "%s%s.md", data_dir, project_name);
	
	char cmd[MAX_CMD];
	snprintf(cmd, MAX_CMD, "%s %s", editor, path);
	system(cmd);
	return 0;
}


/*** Entry Point ***/

int main(int argc, char **argv) {
	if (!todd_init())
		return 1;

	for (int i = 1; i < argc; ++i) {
		if (*argv[i] == '-') {
			++argv[i];
			size_t len = strlen(argv[i]);
			for (size_t j = 0; j < len; ++j, ++argv[i]) {
				switch(*argv[i]) {
					case 'h':
						usage(argv[0]);
						return 0;
					case 'd':
						if (argc > i + 1)
							return daily_note(argv[i + 1]);
						return daily_note(NULL);
					case 'p':
						if (argc >= i + 1)
							return project_note(argv[i + 1]);
						return 1;
					default:
						break;
				}
			}
		}
	}

	return 0;
}
